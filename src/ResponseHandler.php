<?php

namespace RenderScript\Client;

use Infotechnohelp\Filesystem\Filesystem;

class ResponseHandler
{
    private $fs;

    public function __construct(string $outputPath)
    {
        $this->fs = new Filesystem($outputPath);
    }

    public function exec(\stdClass $jsonResponse)
    {
        foreach ($jsonResponse->data as $path => $contents) {
            echo "$path\n";

            $this->fs->write($path, $contents);
        }
    }
}