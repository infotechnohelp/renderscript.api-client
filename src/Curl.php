<?php

namespace RenderScript\Client;

class Curl
{
    private $urlBase;
    private $username;
    private $password;

    private $token;

    public function __construct(string $urlBase, string $username, string $password, string $cachedTokenPath)
    {
        $this->urlBase = $urlBase;
        $this->username = $username;
        $this->password = $password;


        if (file_exists($cachedTokenPath) ) {
            $cachedToken = file_get_contents($cachedTokenPath);
        } else {
            $cachedToken = null;
            
            $dirname = dirname($cachedTokenPath);
            
            if (!is_dir($dirname))
            {
                mkdir($dirname, 0777, true);
            }

            fopen($cachedTokenPath, "w");
        }



        $this->setToken($cachedToken);

        if (!$this->tokenIsValid($cachedToken)) {

            $freshToken = $this->requestToken();

            $this->setToken($freshToken);

            file_put_contents($cachedTokenPath, $freshToken);
        }

    }

    public function setToken(string $token = null)
    {
        $this->token = $token;
    }

    public function requestToken()
    {
        $ch = curl_init();

        $this->setBasicSettings($ch);
        curl_setopt($ch, CURLOPT_URL, "$this->urlBase/token");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');

        $response = curl_exec($ch);
        curl_close($ch);
        $jsonResponse = json_decode($response);

        if ($jsonResponse === null || array_key_exists('error', $jsonResponse)) {
            throw new \Exception($response);
        }

        return $jsonResponse->access_token;
    }

    private function setBasicSettings(&$ch)
    {
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POST, 1);
    }

    private function tokenIsValid(string $token = null)
    {
        $ch = curl_init();

        $this->setBasicSettings($ch);
        curl_setopt($ch, CURLOPT_URL, "$this->urlBase");
        curl_setopt($ch, CURLOPT_POSTFIELDS, "access_token=$token");

        $response = curl_exec($ch);
        $jsonResponse = json_decode($response);
        curl_close($ch);

        if ($jsonResponse === null || array_key_exists('error', $jsonResponse)) {
            return false;
        }

        return $jsonResponse->success;
    }

    public function sendRequest(string $url = null, array $data = [])
    {
        $ch = curl_init();

        $this->setBasicSettings($ch);
        curl_setopt($ch, CURLOPT_URL, "$this->urlBase/$url");

        $data['access_token'] = $this->token;

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $response = curl_exec($ch);

        $jsonResponse = json_decode($response);
        curl_close($ch);

        if ($jsonResponse === null || array_key_exists('error', $jsonResponse) || !$jsonResponse->success) {
            if (empty($response)) {
                throw new \Exception("No response received from '$this->urlBase/$url'");
            }
            throw new \Exception($response);
        }

        return $jsonResponse;
    }
}